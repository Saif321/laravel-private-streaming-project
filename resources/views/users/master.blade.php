<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Course Streaming | @yield('title')</title>
    <link rel="icon" type="image/x-icon" href="{{asset('user_assets/src/assets/img/favicon.ico')}}"/>
    <link href="{{asset('user_assets/layouts/horizontal-dark-menu/css/light/loader.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('user_assets/layouts/horizontal-dark-menu/css/dark/loader.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('user_assets/layouts/horizontal-dark-menu/loader.js')}}"></script>
    @stack('start-style')
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{asset('user_assets/src/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('user_assets/layouts/horizontal-dark-menu/css/light/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('user_assets/layouts/horizontal-dark-menu/css/dark/plugins.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    @stack('end-style')


</head>
<body class="layout-boxed enable-secondaryNav">
    <!-- BEGIN LOADER -->
    <div id="load_screen"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
    </div></div></div>
    <!--  END LOADER -->

   @include('users.components.header')

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>
        @include('users.components.navbar')

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            @yield('layout')
            @include('users.components.footer')
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->
    @stack('start-script')
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{asset('user_assets/src/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('user_assets/src/plugins/src/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('user_assets/src/plugins/src/mousetrap/mousetrap.min.js')}}"></script>
    <script src="{{asset('user_assets/src/plugins/src/waves/waves.min.js')}}"></script>
    <script src="{{asset('user_assets/layouts/horizontal-dark-menu/app.js')}}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    
    @stack('end-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
         $.ajax({
            type: "GET",
            url: "{{ route('categories.index') }}",

            success: function(data) {
                console.log('data',data);
                $('#accordionExample').empty();
                data.forEach(function(element, index) {
                    console.log(element, index);

                    var menu = `<li class="menu ${index === 0 ? 'active' : ''}">
                                    <a href="#dashboard" data-bs-toggle="dropdown" aria-expanded="true" class="dropdown-toggle">
                                        <div class="">
                                            
                                            <span>${element.title}</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu menu-heading">
                                    <div class="heading">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-minus">
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                        <span>APPLICATIONS</span>
                                    </div>
                                </li>`;

                    $('#accordionExample').append(menu);
                    // You can perform operations on each element here
                });
                
            },
            error: function(error) {
                Snackbar.show({
                    text: 'Somthing Went Wrong',
                    pos: 'top-right',
                    actionTextColor: '#fff',
                    backgroundColor: '#e7515a'
                });
            }
        });
    </script>
</body>
</html>
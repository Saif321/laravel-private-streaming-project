<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Your Website</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .navbar-nav .dropdown-submenu {
            position: relative;
        }

        .navbar-nav .dropdown-submenu > .dropdown-menu {
            top: 0;
            left: 100%;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            @foreach($categories as $category)
                <li class="nav-item dropdown clickable">
                    <a class="nav-link {{count($category->children)?'dropdown-toggle':''}}" id="navbarDropdownMenuLink_{{ $category->id }}"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ $category->title }}
                    </a>
                    @if(count($category->children))
                        @include('layouts.partials.dropdown', ['categories' => $category->children])
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</nav>

<script src="{{ asset('assets/assets/js/libs/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

<script>
    $(document).ready(function () {
        // Handle top-level dropdown
        $('.navbar-nav .dropdown').on('mouseenter', function () {
            $(this).find('> ul').stop(true, true).fadeIn('fast').addClass('show');
        }).on('mouseleave', function () {
            $(this).find('> ul').stop(true, true).fadeOut('fast', function () {
                $(this).removeClass('show');
            });
        });

        // Handle submenus
        $('.navbar-nav .dropdown-submenu').on('mouseenter', function () {
            $(this).find('> ul').stop(true, true).fadeIn('fast').addClass('show');
        }).on('mouseleave', function () {
            $(this).find('> ul').stop(true, true).fadeOut('fast', function () {
                $(this).removeClass('show');
            });
        });
    });
</script>
</body>
</html> -->




<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Page Title</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <style>
                          body {
            background-color: #343a40;
            color: #ffffff;
        }

        .navbar {
            background-color: #1b1e21;
        }

        .navbar-dark .navbar-nav .nav-link {
            color: #ffffff;
        }

        .navbar-dark .navbar-toggler-icon {
            background-color: #ffffff;
        }

        main {
            color: #ffffff;
            margin-top: 20px;
        }

        h1,
        h5 {
            color: #17a2b8;
        }

        .nav-tabs .nav-link {
            color: #17a2b8;
        }

        .nav-tabs .nav-link.active {
            background-color: #1b1e21;
            border-color: #1b1e21 #1b1e21 #ffffff;
        }

        .card {
            background-color: #1b1e21;
            border: 1px solid #343a40;
            color: #ffffff;
        }

        .footer {
            background-color: #1b1e21;
            color: #ffffff;
            margin-top: 20px;
            padding: 20px 0;
        }

        .navbar-brand {
            margin-right: 20px;
        }

        .card {
            background-color: #1b1e21;
            border: 1px solid #343a40;
            color: #ffffff;
            height: 100%;
        }

        .card-img-top {
            max-width: 30%;
            height: auto;
        }

        .card-body {
            padding: 1rem;
        }

        .navbar-brand {
            margin-right: 20px;
        }

        .card {
            background-color: #1b1e21;
            border: 1px solid #343a40;
            color: #ffffff;
            height: 100%;
        }

        .card-img-top {
            max-width: 30%;
            height: auto;
        }

        .card-body {
            padding: 1rem;
        }

        .navbar-brand {
            margin-right: 20px;
        }

        .card {
            background-color: #1b1e21;
            border: 1px solid #343a40;
            color: #ffffff;
            height: 100%;
        }

        .card-img-top {
            max-width: 30%;
            height: auto;
        }

        .card-body {
            padding: 1rem;
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Your Brand</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Topic 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Topic 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Topic 3</a>
                </li>
            </ul>
        </div>
    </nav>

    <main class="container mt-4">
        <h1 class="mb-4">Heading for Selected Topic</h1>
        <p class="lead">Description of the selected topic.</p>

        <ul class="nav nav-tabs mb-4">
            <li class="nav-item">
                <a class="nav-link active" href="#">Tab 1</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Tab 2</a>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card d-flex flex-row">
                    <img src="{{asset('assets/assets/img/90x90.jpg')}}" class="card-img-top" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Card Title 1</h5>
                        <p class="card-text"><span>Series: Series 1</span> <span>Video Count: 10</span></p>
                    </div>
                </div>
            </div>
            <!-- Repeat the above card structure for other cards -->
        </div>
    </main>

    <footer class="footer mt-5 bg-dark py-4 text-light">
        <div class="container">
            <p class="text-center">Copyright &copy; Your Website 2023</p>
        </div>
    </footer>

    <script src="{{ asset('assets/assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Additional scripts can be added here -->
</body>

</html>

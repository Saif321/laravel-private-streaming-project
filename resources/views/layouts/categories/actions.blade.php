
<a  onclick='ActiveCategory({{ $category->id }})' type="button" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i></a>
<a onclick='DisableCategory({{ $category->id }})' type="button" class="btn btn-warning"><i class="fa-regular fa-circle-xmark" aria-hidden="true"></i></a>
<a href="{{route('admin.categories.edit', $category->id)}}" type="button" class="btn btn-success"><i class="fa fa-edit" aria-hidden="true"></i></a>
<a onclick='DeleteCategory({{ $category->id }})' type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>


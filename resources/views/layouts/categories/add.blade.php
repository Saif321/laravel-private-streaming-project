@extends('master')

@section('title')
    {{isset($category)?'Update ':'Add ' }}Category
@stop
@section('header')
    {{isset($category)?'Update':'Add' }} Category
@stop
@push('end-style')
    <link href="{{ asset('assets/assets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('end-script')
    <script src="{{ asset('assets/assets/js/scrollspyNav.js') }}"></script>
    <script src="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.js') }}"></script>

    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
        //Second upload
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>
    <script>
        $(document).ready(function() {
            //set initial state.
            $('#is_popular').prop('checked', false);
            $('#is_parent').prop('checked', false);

           var category = @json($category ?? null);

            if (typeof category !== 'undefined' && category.icon) {
                // generate the base URL dynamically
                var baseUrl = '{{ url("/") }}';
                var fullIconUrl = baseUrl + '/' + category.icon;
                // Change the background image to the full icon URL
                $('.custom-file-container__image-preview').css('background', 'url(' + fullIconUrl + ')');
                $('.custom-file-container__image-preview').css('background-repeat', 'no-repeat');
                $('.custom-file-container__image-preview').css('background-size', 'contain');
                $('.custom-file-container__image-preview').css('background-position', 'center');
            }
        });

    </script>
@endpush

@section('content')

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-top-spacing">

                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <div class="widget-content widget-content-area br-6">


                        <form action="{{ isset($category) ? route('admin.categories.update', $category->id) : route('admin.categories.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            @if(isset($category))
                                @method("PUT")
                            @endif

                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter title" name="title" value="{{ old('title', isset($category) ? $category->title : '') }}" required>
                            </div>

                            <div class="form-group">
                                <label for="color">Color</label>
                                <input type="color" class="form-control" id="color" name="color" value="{{ old('color', isset($category) ? $category->color : '#000000') }}" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Parent Category</label>
                                <select class="form-control basic" name="parent_id">
                                    <option disabled selected value> -- Select Parent if any -- </option>
                                    @foreach ($categories as $option)
                                        <option value="{{ $option->id }}" {{ old('parent_id', isset($category) ? ($category->parent_id == $option->id ? 'selected' : '') : '') }}>{{ $option->title }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="custom-file-container" data-upload-id="myFirstImage">
                                <label>Upload (Single File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                <label class="custom-file-container__custom-file">
                                    <input name="image" type="file" class="custom-file-container__custom-file__custom-file-input" accept="image/*" required>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                </label>
                                <div class="custom-file-container__image-preview"></div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>

            </div>

        </div>

    </div>


@stop

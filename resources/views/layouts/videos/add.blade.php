@extends('master')

@section('title')
    {{isset($video)?'Update ':'Add ' }}video
@stop
@section('header')
    {{isset($video)?'Update':'Add' }} video
@stop
@push('end-style')
    <link href="{{ asset('assets/assets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('end-script')
    <script src="{{ asset('assets/assets/js/scrollspyNav.js') }}"></script>
    <script src="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.js') }}"></script>

    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
        //Second upload
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>
    <script>
        $(document).ready(function() {
            //set initial state.
            $('#is_popular').prop('checked', false);
            $('#is_parent').prop('checked', false);

           var video = @json($video ?? null);
           console.log(video);

             if (typeof video !== 'undefined' && video.video_url) {
                 // generate the base URL dynamically
                 var baseUrl = '{{ url("/") }}';
                 var fullIconUrl = baseUrl + '/' + video.video_url;
                 // Change the background image to the full icon URL
                 $('#videoPreview source').attr('src',fullIconUrl);
                 // Load the video
                $('#videoPreview')[0].load();
                //  $('.custom-file-container__image-preview').css('background', 'url(' + fullIconUrl + ')');
                //  $('.custom-file-container__image-preview').css('background-repeat', 'no-repeat');
                //  $('.custom-file-container__image-preview').css('background-size', 'contain');
                //  $('.custom-file-container__image-preview').css('background-position', 'center');
             }
        });

        document.addEventListener('DOMContentLoaded', function () {
            // Get the file input and video element
            var fileInput = document.querySelector('.custom-file-container__custom-file__custom-file-input');
            var videoPreview = document.getElementById('videoPreview');

            // Listen for the change event on the file input
            fileInput.addEventListener('change', function () {
                // Check if a file is selected
                if (fileInput.files.length > 0) {
                    var file = fileInput.files[0];

                    // Create a URL for the selected file
                    var objectURL = URL.createObjectURL(file);

                    // Update the source of the video element
                    videoPreview.src = objectURL;
                }
            });

            // Get the clear button and listen for the click event
            var clearButton = document.querySelector('.custom-file-container__image-clear');
            clearButton.addEventListener('click', function () {
                // Clear the file input and reset the video element source
                fileInput.value = '';
                videoPreview.src = '#';
            });
        });

    </script>
@endpush

@section('content')

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-top-spacing">

                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <div class="widget-content widget-content-area br-6">


                    <form action="{{ isset($video) ? route('admin.videos.update', $video->id) : route('admin.videos.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        @if(isset($video))
                            @method("PUT")
                        @endif

                        <div class="row">
                            <div class="form-group col-6">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter title" name="title" value="{{ old('title', isset($video) ? $video->title : '') }}" {{ isset($video) ? '' : 'required' }}>
                                @error('title')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-6">
                                <label for="exampleInputEmail1">Series</label>
                                <select class="form-control basic" name="series_id">
                                    <option disabled selected value> -- Select Series-- </option>
                                    @foreach ($series as $option)
                                        <option value="{{ $option->id }}" {{ old('series_id', isset($video) ? ($video->series_id == $option->id ? 'selected' : '') : '') }}>{{ $option->title }}</option>
                                    @endforeach
                                </select>
                                @error('series_id')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-12">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="description" cols="30" rows="10">{{ old('description', isset($video) ? $video->description : '') }}</textarea>
                                @error('description')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="custom-file-container col-12" data-upload-id="myVideo">
                                <label>Upload Video <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Video">x</a></label>
                                <label class="custom-file-container__custom-file">
                                    <input name="video_file" type="file" class="custom-file-container__custom-file__custom-file-input" accept="video/*" {{ isset($video) ? '' : 'required' }}>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="104857600" />
                                    <span class="custom-file-container__custom-file__custom-file-control">Choose File</span>
                                </label>
                                <div class="custom-file-container__video-preview mt-3">
                                    <video id="videoPreview" controls width="100%" height="auto">
                                        <source src="#" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                                @error('video_file')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-12">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>


                    </div>
                </div>

            </div>

        </div>

    </div>


@stop

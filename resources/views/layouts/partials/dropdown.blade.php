<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink_{{ $categories->first()->parent_id }}">
    @foreach($categories as $category)
        <li class="dropdown-submenu clickable">
            <a class="dropdown-item {{count($category->children)?'dropdown-toggle':''}}"
               href="#">{{ $category->title }}</a>
            @if(count($category->children))
                @include('layouts.partials.dropdown', ['categories' => $category->children])
            @endif
        </li>
    @endforeach
</ul>
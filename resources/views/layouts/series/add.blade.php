@extends('master')

@section('title')
    {{isset($series)?'Update ':'Add ' }}Series
@stop
@section('header')
    {{isset($series)?'Update':'Add' }} Series
@stop
@push('end-style')
    <link href="{{ asset('assets/assets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('end-script')
    <script src="{{ asset('assets/assets/js/scrollspyNav.js') }}"></script>
    <script src="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.js') }}"></script>

    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
        //Second upload
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>
    <script>
        $(document).ready(function() {
            //set initial state.
            $('#is_popular').prop('checked', false);
            $('#is_parent').prop('checked', false);

           var series = @json($series ?? null);

            if (typeof series !== 'undefined' && series.image) {
                // generate the base URL dynamically
                var baseUrl = '{{ url("/") }}';
                var fullIconUrl = baseUrl + '/' + series.image;
                // Change the background image to the full icon URL
                $('.custom-file-container__image-preview').css('background', 'url(' + fullIconUrl + ')');
                $('.custom-file-container__image-preview').css('background-repeat', 'no-repeat');
                $('.custom-file-container__image-preview').css('background-size', 'contain');
                $('.custom-file-container__image-preview').css('background-position', 'center');
            }
        });

    </script>
@endpush

@section('content')

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-top-spacing">

                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <div class="widget-content widget-content-area br-6">


                        <form action="{{ isset($series) ? route('admin.series.update', $series->id) : route('admin.series.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            @if(isset($series))
                                @method("PUT")
                            @endif

                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter title" name="title" value="{{ old('title', isset($series) ? $series->title : '') }}" required>
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="description" cols="30" rows="10">{{ old('description', isset($series) ? $series->description : '') }}</textarea>
                                @error('description')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>




                            <div class="form-group">
                                <label for="exampleInputEmail1">Course</label>
                                <select class="form-control basic" name="course_id">
                                    <option disabled selected value> -- Select Course if any -- </option>
                                    @foreach ($courses as $option)
                                        <option value="{{ $option->id }}" {{ old('course_id', isset($series) ? ($series->course_id == $option->id ? 'selected' : '') : '') }}>{{ $option->title }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="custom-file-container" data-upload-id="myFirstImage">
                                <label>Upload (Single File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                <label class="custom-file-container__custom-file">
                                    <input name="image" type="file" class="custom-file-container__custom-file__custom-file-input" accept="image/*" required>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                </label>
                                <div class="custom-file-container__image-preview"></div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>

            </div>

        </div>

    </div>


@stop

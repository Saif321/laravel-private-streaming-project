<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Video;
use App\Models\Series;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{

    private function make_directory_path($category)
    {
        $path = str_replace(' ', '_', trim($category->title)); // Trim leading and trailing spaces and replace spaces with underscores

        if ($category->parent) {
            $parentPath = $this->make_directory_path($category->parent);
            $path = $parentPath . '/' . $path;
        }

        return trim($path, '/');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            $videos = Video::with('series')->get();

            return DataTables::of($videos)
                ->addIndexColumn()
                ->editColumn('title', function ($video) {
                    return ucfirst($video->title);
                })
                // Add other columns as needed
                ->editColumn('category', function ($video) {
                    return ($video->series_id) ? ucfirst($video->series->title) : 'N/A';
                })
                ->editColumn('video', function ($video) {
                    return "<a href='".$video->video_url."'>video</a>";
                })
                ->editColumn('actions', function ($video) {
                    return view('layouts.videos.actions', compact('video'));
                })
                ->rawColumns(['actions','video'])
                ->toJson();
        }

        return view('layouts.videos.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $series = Series::orderBy('title', 'ASC')->get();
        return view('layouts.videos.add', compact('series'));
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     $request->validate([
    //         'series_id' => 'required|exists:categories,id',
    //         'title' => 'required|string',
    //         'description' => 'required|string',
    //         'video_file' => 'required|mimetypes:video/*',
    //     ]);
    
    //     $category = Category::find($request->series_id);
    
    //     $dir_path = $this->make_directory_path($category);
    
    //     $file = $request->file('video_file');
    //     $fileName = $file->hashName(); // Use a hashed name for the file
    //     $filePath = $dir_path . '/' . $fileName; // Append the file name to the directory path
    
    //     // Store the video file in S3 with the modified file path
    //     Storage::disk('s3')->put($filePath, $request->file('video_file'));
    
    //     $videoUrl = Storage::disk('s3')->url($filePath);
    
    //     Video::create([
    //         'series_id' => $request->input('series_id'),
    //         'title' => $request->input('title'),
    //         'description' => $request->input('description'),
    //         'video_url' => $videoUrl,
    //         // Add other fields as needed
    //     ]);
    
    //     return redirect()->route('admin.videos.index')->with('success', 'Video created successfully!');
    // }

    // public function store(Request $request)
    // {
    //     $request->validate([
    //         'series_id' => 'required|exists:categories,id',
    //         'title' => 'required|string',
    //         'description' => 'required|string',
    //         'video_file' => 'required|mimetypes:video/*', // Assuming 'video_file' is the name attribute of your file input
    //         // Add other validation rules as needed
    //     ]);
    //     // dump(env('AWS_ACCESS_KEY_ID'));
    //     // dump(env('AWS_SECRET_ACCESS_KEY'));
    //     // dump(env('AWS_DEFAULT_REGION'));
    //     // dump(env('AWS_BUCKET'));
    //     // dump(env('AWS_USE_PATH_STYLE_ENDPOINT'));
        
    //     $category = Category::find($request->series_id);


    //     $dir_path = $this->make_directory_path($category);

    //     $file = $request->file('video_file');
    //     $fileName = $file->hashName();
    //     try {
    //         $filePath = Storage::disk('s3')->put('/', $request->file('video_file'));

    //         $videoUrl = Storage::disk('s3')->url($filePath);
    
    //         Video::create([
    //             'series_id' => $request->input('series_id'),
    //             'title' => $request->input('title'),
    //             'description' => $request->input('description'),
    //             'video_url' => $videoUrl,
    //             // Add other fields as needed
    //         ]);
    //     }catch(\Exception $e){
    //         return $e->getMessage();
    //     }
       

    //     return redirect()->route('admin.videos.index')->with('success', 'Video created successfully!');
    // }
    public function store(Request $request)
    {
        $request->validate([
            'series_id' => 'required|exists:categories,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'video_file' => 'required|mimetypes:video/*',
        ]);

        $category = Series::find($request->series_id);

        $dir_path = $this->make_directory_path($category);
        //local storage code
        // $file      = $request->file('video_file');
        // $file_path = Storage::put('public/'.$dir_path, $file);
        // $file_path = str_replace("public","storage",$file_path);
        // $videoUrl = $file_path;

        //S3 storage code
        $file = $request->file('video_file');
        $fileName = $file->hashName();
        $filePath = $dir_path.'/'.$fileName;
                
        Storage::disk('s3')->put($filePath, file_get_contents($file));
        // Storage::disk('s3')->assertExists($filePath,300);
        $videoUrl = Storage::disk('s3')->url($filePath);

        Video::create([
            'series_id' => $request->input('series_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'video_url' => $videoUrl,
            // Add other fields as needed
        ]);
        // } catch (\Exception $e) {
        //     return $e->getMessage();
        // }

        return redirect()->route('admin.videos.index')->with('success', 'Video created successfully!');
    }


    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $video = Video::find($id);

        return view('layouts.videos.show', compact('video'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        
        $video = Video::find($id);
        $series = Series::orderBy('title', 'ASC')->get();

        return view('layouts.videos.add', compact('series', 'video'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'series_id' => 'sometimes|exists:categories,id',
            'title' => 'sometimes|string',
            'description' => 'sometimes|string',
            'video_file' => 'sometimes|mimetypes:video/*',
            // Add other validation rules as needed
        ]);
    
        $video = Video::find($id);
    
        if ($request->hasFile('video_file')) {
            $file = $request->file('video_file');
            $fileName = $file->hashName();
            $filePath = $dir_path . '/' . $fileName;
    
            // Delete old video file if it exists
            if (Storage::disk('s3')->exists($video->video_url)) {
                Storage::disk('s3')->delete($video->video_url);
            }
    
            Storage::disk('s3')->put($filePath, file_get_contents($file));
            $request['video_url'] = Storage::disk('s3')->url($filePath);
        }
    
        $video->update($request->except('video_file', '_token'));
    
        return redirect()->route('admin.videos.index')->with('success', 'Video updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $video = Video::find($id);
        $video->delete();

        return redirect()->route('admin.videos.index')->with('success', 'Video deleted successfully!');
    }


}
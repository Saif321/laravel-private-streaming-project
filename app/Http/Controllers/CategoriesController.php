<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        
        if (request()->ajax()) {
            $categories = Category::with('parent')->get();
            // dd($categories);
            return DataTables::of($categories)
                ->addIndexColumn()
                ->editColumn('title', function ($category) {
                    // dd($category);
                    return ucfirst($category->title);
                })
                ->editColumn('color', function ($category) {
                    return ($category->color
                        ? "<span style='background-color:{$category->color};color:#ffffff' class='badge'>" . ucfirst($category->color) . "</span>"
                        : "<span style='background-color:#000000;color:#ffffff' class='badge'>000000</span>"
                    );
                })
                
                ->editColumn('icon', function ($category) {
                    return view('layouts.categories.icon', compact('category'));
                })
                ->editColumn('parent', function ($category) {
                    return ($category->parent_id)?ucfirst($category->parent->title):'N/A';
                })
                ->editColumn('actions', function ($category) {
                    return view('layouts.categories.actions', compact('category'));
                })
                ->rawColumns(['actions', 'status', 'avatar', 'color'])
                ->toJson();
        }
        return view('layouts.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::orderBy('title','ASC')->get();
        return view('layouts.categories.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate the request
        $request->validate([
            'title' => 'required|unique:categories|string',
            'color' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:10485760', // Adjust image validation rules as needed
        ]);

        $requestData = $request->except('_token', 'MAX_FILE_SIZE', 'image');

        // Check if the request has an image file
        if ($request->hasFile('image')) {
            // Store the image in the storage and get the path
            $file = $request->file('image');
            $file_path = Storage::put('public/category_icon', $file);
            $file_path = str_replace("public", "storage", $file_path);

            // Save the path in the category_icon column
            $requestData['icon'] = $file_path;
        }

        // Create the category with the modified request data
        Category::create($requestData);

        return redirect()->route('admin.categories.index')->with('success', 'Category created successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $category = Category::find($id);
        $categories = Category::orderBy('title','ASC')->get();
        return view('layouts.categories.add', compact('categories','category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

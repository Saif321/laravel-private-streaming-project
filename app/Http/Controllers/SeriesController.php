<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Series;
use Illuminate\Support\Facades\Storage;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            $series = Series::with('course')->get();

            return DataTables::of($series)
                ->addIndexColumn()
                ->editColumn('title', function ($series) {
                    return ucfirst($series->title);
                })
                ->editColumn('description', function ($series) {
                    return substr($series->description, 0, 50) . '...'; // Display a truncated version of the description
                })
                ->editColumn('icon', function ($series) {
                    return view('layouts.series.icon', compact('series'));
                })
                ->editColumn('course', function ($series) {
                    return ($series->course) ? ucfirst($series->course->title) : 'N/A';
                })
                ->editColumn('actions', function ($series) {
                    // Adjust this as needed based on your actions view for series
                    return view('layouts.series.actions', compact('series'));
                })
                ->rawColumns(['icon', 'actions'])
                ->toJson();
        }

        return view('layouts.series.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::orderBy('title', 'ASC')->get();
        return view('layouts.series.add', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'course_id' => 'required|exists:courses,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:10485760', // Adjust image validation rules as needed
        ]);
        $requestData = $request->except('_token', 'MAX_FILE_SIZE', 'image');

        // Check if the request has an image file
        if ($request->hasFile('image')) {
            // Store the image in the storage and get the path
            $file = $request->file('image');
            $file_path = Storage::put('public/series_image', $file);
            $file_path = str_replace("public", "storage", $file_path);

            // Save the path in the image column
            $requestData['image'] = $file_path;
        }

        Series::create($requestData);

        return redirect()->route('admin.series.index')->with('success', 'Series created successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $series = Series::find($id);
        $courses = Course::orderBy('title', 'ASC')->get();
        return view('layouts.series.add', compact('courses','series'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            $courses = Course::with('category')->get();

            return DataTables::of($courses)
                ->addIndexColumn()
                ->editColumn('title', function ($course) {
                    return ucfirst($course->title);
                })
                ->editColumn('icon', function ($course) {
                    return view('layouts.courses.icon', compact('course'));
                })
                ->editColumn('category', function ($course) {
                    return ($course->category) ? ucfirst($course->category->title) : 'N/A';
                })
                ->editColumn('actions', function ($course) {
                    return view('layouts.courses.actions', compact('course'));
                })
                ->rawColumns(['actions'])
                ->toJson();
        }

        return view('layouts.courses.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::orderBy('title', 'ASC')->get();
        return view('layouts.courses.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|string',
            'icon' => 'required|image|mimes:jpeg,png,jpg,gif|max:10485760', // Adjust image validation rules as needed
        ]);

        $requestData = $request->except('_token', 'MAX_FILE_SIZE', 'icon');

        // Check if the request has an icon file
        if ($request->hasFile('icon')) {
            // Store the icon in the storage and get the path
            $file = $request->file('icon');
            $file_path = Storage::put('public/course_icon', $file);
            $file_path = str_replace("public", "storage", $file_path);

            // Save the path in the icon column
            $requestData['icon'] = $file_path;
        }

        Course::create($requestData);

        return redirect()->route('admin.courses.index')->with('success', 'Course created successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function edit(string $id)
    {
        $course = Course::find($id);
        $categories = Category::orderBy('title','ASC')->get();
        return view('layouts.courses.add', compact('categories','course'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

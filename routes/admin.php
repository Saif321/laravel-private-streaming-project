<?php

use App\Http\Controllers\{CategoriesController,CourseController,SeriesController,VideoController};
use Illuminate\Support\Facades\Route;
use App\Models\Category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::get('/dashboard', function () {
    return view('layouts.dashboard');
})->middleware(['auth', 'verified'])->name('admin.dashboard');

Route::middleware('auth')->group(function () {
    Route::resource('categories', CategoriesController::class)->names('admin.categories');
    Route::resource('courses', CourseController::class)->names('admin.courses');
    Route::resource('series', SeriesController::class)->names('admin.series');
    Route::resource('videos', VideoController::class)->names('admin.videos');
});



